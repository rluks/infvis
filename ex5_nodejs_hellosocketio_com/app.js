var express = require("express"),
    app = require("express")(),
    http = require("http").Server(app);

var io = require('socket.io')(http);
var d3 = require("d3");
var fs = require('fs');
	
var dataArray = new Array();

fs.readFile('summary_donors.json', 'utf8', function (err, data) {
    if (err) throw err; // we'll not consider error handling for now
	dataArray["donors"] = data;
});	


//add more data loading:
/*
fs.readFile('filename', 'utf8', function (err, data) {
    if (err) throw err; // we'll not consider error handling for now
	dataArray["index_for_requesting_it_with_client"] = data;
});	*/
	
http.listen(2500, function() {
    console.log("Connected to :2500");
});

app.use(express.static(__dirname));

io.on('connection', function (socket) {
	
	socket.on('getdata', function (receivedData) {
		socket.emit(receivedData.reqdata, { data : dataArray[receivedData.reqdata] });
	});
	
	socket.on('disconnect', function () {
		io.emit('user disconnected');
	});
});





