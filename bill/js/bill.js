d3.select('#slider8')
    .call(d3.slider()
        .value(100)
        .orientation("vertical")
        .step(0.01)
        .on("slide", function(evt, value) {
            //console.log(value)
            updateDonationTable(value);
        })
    );

function updateDonationTable(value){
    var table = document.getElementById("donation-table");
    var innerHtml = "";
    //value: 0 = poor, 100 = rich
    var billGivingPrcnt = 100 - value;
    var AustriaPop = 8572895;
    var EuropePop = 508000000;
    var DACHPop = 98026432;
    var ourHalf = 39750000000;
    var moneyPrcnt = ourHalf * billGivingPrcnt/100;
    var billGivingUSD = 2 * moneyPrcnt;
    var eachAustriaUSD =  Math.round(moneyPrcnt/AustriaPop);
    var eachEuropeUSD = Math.round(moneyPrcnt/EuropePop);
    var eachDACHUSD = Math.round(moneyPrcnt/DACHPop);

    if(value == 0) {
        innerHtml = "<h3>Success! We bankrupt Bill!</h3>";
        //sad head image
        document.getElementById("handle-one").style.backgroundImage = 'url(bill_sad.png)';
        document.body.style.backgroundColor = "#fbfbfb";
    }else{
        //default head image
        document.getElementById("handle-one").style.backgroundImage = 'url(bill.png)';
        document.body.style.backgroundColor = "#f7f7f7";
    }

    innerHtml += "Bill is giving away <b>" + billGivingPrcnt + "%</b> of his money"
    innerHtml += " (" + billGivingUSD + "$).";
    innerHtml += "</br>";
    innerHtml += "Each citizen of Austria gives <b>" + eachAustriaUSD + "$</b>";
    innerHtml += " or</br>";
    innerHtml += "Each European gives " + eachEuropeUSD + "$";
    innerHtml += " or</br>";
    innerHtml += "Each person in DACH gives " + eachDACHUSD + "$.";

    table.innerHTML = innerHtml;
}