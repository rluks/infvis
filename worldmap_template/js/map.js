
d3.select(window).on("resize", throttle);

var zoom = d3.behavior.zoom()
    .scaleExtent([1, 9])
    .on("zoom", move);


var width = document.getElementById('container').offsetWidth;

var height = width / 2;

var topo,projection,path,svg,g;

var graticule = d3.geo.graticule();

var tooltip = d3.select("#container").append("div").attr("class", "tooltip hidden");

setup(width,height);

function setup(width,height){
    projection = d3.geo.mercator()
        .translate([(width/2), (height/2)])
        .scale( width / 2 / Math.PI);

    path = d3.geo.path().projection(projection);

    svg = d3.select("#container").append("svg")
        .attr("width", width)
        .attr("height", height)
        .call(zoom)
        .on("click", click)
        .append("g");

    g = svg.append("g");

}

d3.json("data/world-topo-min.json", function(error, world) {

    var countries = topojson.feature(world, world.objects.countries).features;

    topo = countries;
    draw(topo);

});


//Reading and saving wpvCases
var listOfCases=[];
d3.json("data/wpvCases.json",function(error,data) {
    if (error) throw error;
    data.forEach(function (entry) {
        var caseYear = {}
        caseYear['dataType'] = 'case';
        caseYear['year'] = entry.YEAR;
        caseYear['value'] = entry;
        //caseYear['AFG'] = entry.AFG;
        //caseYear['PAK'] = entry.PAK;
        //caseYear['NGR'] = entry.NGR;
        //caseYear['GUI'] = entry.GUI;
        //caseYear['LAO'] = entry.LAO;
        //caseYear['MAD'] = entry.MAD;
        //caseYear['UKR'] = entry.UKR;
        //caseYear['CMR'] = entry.CMR;
        //caseYear['GEQ'] = entry.GEQ;
        //caseYear['ETH'] = entry.ETH;
        //caseYear['IRQ'] = entry.IRQ;
        //caseYear['ISR'] = entry.ISR;
        //caseYear['SOM'] = entry.SOM;
        //caseYear['SYR'] = entry.SYR;
        //caseYear['SUD'] = entry.SUD;


        listOfCases.push(caseYear);
        //console.log('reading the wpvCASES');
        //console.log(d);
    });
    console.log(listOfCases);
});

var wpvCasesYear = {};
d3.json("data/wpvCases_yearIndex.json",function(error,data) {
    if (error) throw error;
	wpvCasesYear = data;
    console.log(wpvCasesYear);
});

var donationsCountries = {};
d3.json("data/donations_countries.json",function(error,data) {
    if (error) throw error;
	donationsCountries = data;
});

//https://en.wikipedia.org/wiki/List_of_IOC_country_codes
var caseCountries=["AFG","PAK","NGR","GUI","LAO","MAD","UKR","CMR","GEQ","ETH","IRQ","ISR","SOM","SYR","SUD"];
var assocCountries = {};
assocCountries["Afghanistan"] = "AFG";
assocCountries["Pakistan"] = "PAK";
assocCountries["Nigeria"] = "NGR";
assocCountries["Guinea"] = "GUI";
assocCountries["Laos"] = "LAO";
assocCountries["Madagascar"] = "MAD";
assocCountries["Ukraine"] = "UKR";
assocCountries["Cameroon"] = "CMR";
assocCountries["Equatorial Guinea"] = "GEQ";
assocCountries["Ethiopia"] = "ETH";
assocCountries["Iraq"] = "IRQ";
assocCountries["Israel"] = "ISR";
assocCountries["Somalia"] = "SOM";
assocCountries["Syria"] = "SYR";
assocCountries["South Sudan"] = "SUD";

var donationCountries=["USA","GBR","JPN","GER","CAN","ITA","RUS","NOR","AUS","AUT","IND"];

assocCountries["United States"] = "USA";
assocCountries["Great Britain"] = "GBR";
assocCountries["Japan"] = "JPN";
assocCountries["Germany"] = "GER";
assocCountries["Canada"] = "CAN";
assocCountries["Italy"] = "ITA";
assocCountries["Russia"] = "RUS";
assocCountries["Norway"] = "NOR";
assocCountries["Australia"] = "AUS";
assocCountries["Austria"] = "AUT";
assocCountries["India"] = "IND";

//offsets for tooltips
var offsetL = document.getElementById('container').offsetLeft+20;
var offsetT = document.getElementById('container').offsetTop+10;


function updateMap(){
    var country = g.selectAll(".country-associated");//.data(topo);
    country.style("fill", function(d, i) { return getCountryColor(d.properties.name) })

    country
        .on("mousemove", function(d,i) {

            var mouse = d3.mouse(svg.node()).map( function(d) { return parseInt(d); } );

            var ttNumCases = "0";
            var ttDonated = "0";

            for(var j=0; j<caseCountries.length; j++){
                if(assocCountries[d.properties.name] == caseCountries[j]){
                    var countryCode =caseCountries[j];
                    var numCases = -1;
                    for(var k = 0; k < listOfCases.length; k++)
                    {

                        if(listOfCases[k].year == yearSelectedBySlider)
                        {
                            var obj = listOfCases[k].value;
                            numCases = obj[countryCode];
                        }
                    }

                    ttNumCases = numCases;
                }
            }

            ttDonated = GetDonatedForCountry(d.properties.name);

            tooltip.classed("hidden", false)
                .attr("style", "left:"+(mouse[0]+offsetL)+"px;top:"+(mouse[1]+offsetT)+"px")
                .html(
                    d.properties.name +" </br>"+ "WPV Cases:" + ttNumCases
                    + "</br>" + "$" + ttDonated + "M"
                );

        });
}

function getCountryClass(countryName){
    var countryCode = assocCountries[countryName];

    if(typeof countryCode === 'undefined')
        return "country";
    else
        return "country country-associated";
}


function draw(topo) {

    svg.append("path")
        .datum(graticule)
        .attr("class", "graticule")
        .attr("d", path);


    g.append("path")
        .datum({type: "LineString", coordinates: [[-180, 0], [-90, 0], [0, 0], [90, 0], [180, 0]]})
        .attr("class", "equator")
        .attr("d", path);


    var country = g.selectAll(".country").data(topo);

    country.enter().insert("path")
        .attr("class", function(d, i) { return getCountryClass(d.properties.name) })
        //.attr("class", "test")
        .attr("d", path)
        .attr("id", function(d,i) { return d.id; })
        .attr("title", function(d,i) {
            return d.properties.name;
            //for(var j=0; j<caseCountries.length; j++){
            //   if(d.properties.name == caseCountries[j]){
            //       return (d.properties.name + " WPV cases" + "TEST");
            //   }
            //}
        })
        //.style("fill", function(d, i) { return d.properties.color; });
		.style("fill", function(d, i) { return getCountryColor(d.properties.name) })
        .on("click", function(d){
            selectCountry(d.properties.name);
        });



    //tooltips
    country
        .on("mousemove", function(d,i) {

            var mouse = d3.mouse(svg.node()).map( function(d) { return parseInt(d); } );

            var ttNumCases = "0";
			var ttDonated = "0";

            for(var j=0; j<caseCountries.length; j++){
                //console.log(assocCountries[d.properties.name]);
                if(assocCountries[d.properties.name] == caseCountries[j]){
                    var countryCode =caseCountries[j];
                    var numCases = -1;
                    for(var k = 0; k < listOfCases.length; k++)
                    {

                        if(listOfCases[k].year == yearSelectedBySlider)
                        {
                            var obj = listOfCases[k].value;
                            //console.log(obj);
                            //console.log(countryCode);
                            numCases = obj[countryCode];
                        }
                    }


                    //console.log(numCases);
                    ttNumCases = numCases;
                    //var attribute = caseCountries[j];
                   //toolTip = toolTip + "WPV Cases:" + obj.attribute;
                    //console.log(toolTip);
                }else{
                    //toolTip = d.properties.name;
                }
            }
			
			ttDonated = GetDonatedForCountry(d.properties.name);

            tooltip.classed("hidden", false)
                .attr("style", "left:"+(mouse[0]+offsetL)+"px;top:"+(mouse[1]+offsetT)+"px")
                .html(
                //d.properties.name + "TEST"
                d.properties.name +" </br>"+ "WPV Cases:" + ttNumCases 
								+ "</br>" + "$" + ttDonated + "M"
                 );

        })
        .on("mouseout",  function(d,i) {
            tooltip.classed("hidden", true);
        })
        ;


    //EXAMPLE: adding some capitals from external CSV file
    d3.csv("data/country-capitals.csv", function(err, capitals) {

        capitals.forEach(function(i){
            addpoint(i.CapitalLongitude, i.CapitalLatitude, i.CapitalName );
        });

    });
}	
	



function redraw() {
    width = document.getElementById('container').offsetWidth;
    height = width / 2;
    d3.select('svg').remove();
    setup(width,height);
    draw(topo);
}


function move() {

    var t = d3.event.translate;
    var s = d3.event.scale;
    zscale = s;
    var h = height/4;


    t[0] = Math.min(
        (width/height)  * (s - 1),
        Math.max( width * (1 - s), t[0] )
    );

    t[1] = Math.min(
        h * (s - 1) + h * s,
        Math.max(height  * (1 - s) - h * s, t[1])
    );

    zoom.translate(t);
    g.attr("transform", "translate(" + t + ")scale(" + s + ")");

    //adjust the country hover stroke width based on zoom level
    d3.selectAll(".country").style("stroke-width", 1.5 / s);

}



var throttleTimer;
function throttle() {
    window.clearTimeout(throttleTimer);
    throttleTimer = window.setTimeout(function() {
        redraw();
    }, 200);
}


//geo translation on mouse click in map
function click() {
    var latlon = projection.invert(d3.mouse(this));
    console.log(latlon);
}


//function to add points and text to the map (used in plotting capitals)
function addpoint(lat,lon,text) {

    var gpoint = g.append("g").attr("class", "gpoint");
    var x = projection([lat,lon])[0];
    var y = projection([lat,lon])[1];

    gpoint.append("svg:circle")
        .attr("cx", x)
        .attr("cy", y)
        .attr("class","point")
        .attr("r", 1.5);

    //conditional in case a point has no associated text
    if(text.length>0){

        gpoint.append("text")
            .attr("x", x+2)
            .attr("y", y+2)
            .attr("class","text")
            .text(text);
    }

}

