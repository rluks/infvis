polioColorValues = [];
polioColorValues[0] = d3.rgb(185,185,185);
polioColorValues[10] = d3.rgb(255,251,127);
polioColorValues[20] = d3.rgb(255,242,0);
polioColorValues[30] = d3.rgb(255,221,0);
polioColorValues[40] = d3.rgb(242,146,84);
polioColorValues[50] = d3.rgb(236,94,54);
polioColorValues[100] = d3.rgb(211,79,29);
polioColorValues[500] = d3.rgb(193,39,45);
polioColorValues[1000] = d3.rgb(177,52,47);
polioColorValues[9000] = d3.rgb(255,10,0);

donationColorValues = [];
donationColorValues[0] = d3.rgb(185,185,185);
donationColorValues[5] = d3.rgb(204,224,237);
donationColorValues[10] = d3.rgb(153,194,219);
donationColorValues[20] = d3.rgb(102,163,202);
donationColorValues[30] = d3.rgb(51,133,184);
donationColorValues[40] = d3.rgb(0,102,166);
donationColorValues[50] = d3.rgb(0,77,125);
donationColorValues[100] = d3.rgb(46,60,184);
donationColorValues[200] = d3.rgb(23,0,173);
donationColorValues[9000] = d3.rgb(43,0,254);

function getCountryColor(countryFullName){
	//return "#98df8a";

	var countryCode = assocCountries[countryFullName];
	var returnColor;
	
	//cases
	var idx = caseCountries.indexOf(countryCode);
	if (idx != -1 && typeof countryCode != 'undefined'){
		var numCases = wpvCasesYear[yearSelectedBySlider][countryCode];
		var prevKey = -1;
		Object.keys(polioColorValues).forEach(function (key) {

		   if(numCases < key && numCases >= prevKey){
			   returnColor = polioColorValues[key];
		   }
		   
		   prevKey = key;
		});
	}else{
		returnColor = polioColorValues["0"];
	}
	
	if (idx != -1)
		return returnColor;
	
	//donations
	idx = donationCountries.indexOf(countryCode);
	if (idx != -1 && typeof countryCode != 'undefined'){
		var donationValue = donationsCountries[yearSelectedBySlider][countryCode];
		var prevKey = -1;
		Object.keys(donationColorValues).forEach(function (key) {

		   if(donationValue < key && donationValue >= prevKey){
			   returnColor = donationColorValues[key];
		   }
		   
		   prevKey = key;
		});
	}else{
		returnColor = donationColorValues["0"];
	}
	
	//console.log(countryCode + " " + returnColor)
	return returnColor;
	
}