var selectedCountries = [];

function selectCountry(countryName){
    var countryCode = assocCountries[countryName];

    if(typeof countryCode === 'undefined')
        return;

    console.log(countryName);

    var idx = caseCountries.indexOf(countryCode);
    if (idx != -1){
        //console.log("polio case country");
        var idxSelected = selectedCountries.indexOf(countryCode);
        if (idxSelected == -1)//add if its not there
            selectedCountries.push(countryCode);
        else
            selectedCountries.splice(idxSelected, 1);//remove if present
    }

    var idxDonate = donationCountries.indexOf(countryCode);
    if (idxDonate != -1) {
        //console.log("donation country");
        var idxSelectedDonate = selectedCountries.indexOf(countryCode);
        if (idxSelectedDonate == -1)//add if its not there
            selectedCountries.push(countryCode);
        else
            selectedCountries.splice(idxSelectedDonate, 1);//remove if present
    }

    console.dir(selectedCountries);
}
