  
var legendsvg = d3.select("#div_legend")
	.append("svg")
	.attr("height", 300);
var dummyDataset = [];

Object.keys(polioColorValues).forEach(function (key) {
	//dummyDataset.push([{x: 0}]);
	dummyDataset.push({number:key, rgb:polioColorValues[key]});
});
dummyDataset.reverse();
	
	var offset = 15;
	var legend = legendsvg.append("g")
	  .attr("class", "legend");

	legend.selectAll('g')
		.data(dummyDataset)
		.enter()
		.append('g')
      .each(function(d, i) {
        var g = d3.select(this);	
		
        g.append("rect")
          .attr("x", offset)		
          .attr("y", i*25)
          .attr("width", 20)
          .attr("height", 20)
          .style("fill", d.rgb);    

		g.append("text")
          .attr("x", offset + 25)
          .attr("y", i * 25 + 15)
          .attr("height",30)
          .attr("width",100)
          .text(legendTextHelper(d.number));		  
      });	  
	  	  
	legend.append('g')
		  .append("text")
          .attr("x", 0)
          .attr("y", 0)
          .attr("height",30)
          .attr("width",100)
		  .attr("transform", "translate(0,20),rotate(90)" )
          .text("Number of Wild Polio Cases");	



function legendTextHelper(number){
	if(number > 1000)
		return ">1000";
	else if(number == 0)
		return "0"
	else
		return "<" + number;
}