/**
 * Created by MIL on 09.01.2016.
 */



(function(){
    window.caseCountGlobal = 0;
    graphNumWPVCases();
    graphDonations();

})();

var caseCountForYear = 0;
function drawKidPictogram(caseCountForYear){
    d3.select("#pictogram").html("");
    //var svgDoc=d3.select("#pictogram").append("svg").attr("viewBox","0 0 293 57"); //factor divide svg by 3.5
    var svgDoc=d3.select("#pictogram").append("svg").attr("viewBox","0 0 429 57"); //factor divide svg by 3.5
    svgDoc.append("defs")
        .append("g")
        .attr("id","iconCustom")
        .append("path")
        .attr("d","M3.5,2H2.7C3,1.8,3.3,1.5,3.3,1.1c0-0.6-0.4-1-1-1c-0.6,0-1,0.4-1,1c0,0.4,0.2,0.7,0.6,0.9H1.1C0.7,2,0.4,2.3,0.4,2.6v1.9c0,0.3,0.3,0.6,0.6,0.6h0.2c0,0,0,0.1,0,0.1v1.9c0,0.3,0.2,0.6,0.3,0.6h1.3c0.2,0,0.3-0.3,0.3-0.6V5.3c0,0,0-0.1,0-0.1h0.2c0.3,0,0.6-0.3,0.6-0.6V2.6C4.1,2.3,3.8,2,3.5,2z");
    svgDoc.append("rect")
        .attr("width",1500)
        //.attr("height",200);
    //specify the number of columns and rows for pictogram layout
    var maxCols = 50;
    if(caseCountForYear < (maxCols * 10)){
        numRows =1;
        numCols = Math.ceil(caseCountForYear/10);
    }else {
        var numCols = 50;
        var numRows = Math.ceil(caseCountForYear / 10 / numCols);
    }

    //padding for grid
    //var xPadding =10;
    //var yPadding =15;
    var xPadding =0;
    var yPadding =10;
    //horizontal and vertical spacing between the icons
    //var hBuffer = 9;
    //var wBuffer = 8;
    var hBuffer = 9;
    var wBuffer = 7;
    var myIndex = d3.range(numRows*numCols);
    //text element to display number of icons highlighted
    svgDoc.append("text")
        .attr("id","txtValue")
        .attr("x",xPadding)
        .attr("y",yPadding)
        .attr("dy",-3)
        .text("WPV Cases")
    //create group element and create an svg <use> element for each icon
    svgDoc.append("g")
        .attr("id","pictoLayer")
        .selectAll("use")
        .data(myIndex)
        .enter()
        .append("use")
        .attr("xlink:href","#iconCustom")
        .attr("id",function(d)    {
            return "icon"+d;
        })
        .attr("x",function(d) {
            var remainder=d % numCols;//calculates the x position (column number) using modulus
            return xPadding+(remainder*wBuffer);//apply the buffer and return value
        })
        .attr("y",function(d) {
            var whole=Math.floor(d/numCols)//calculates the y position (row number)
            return yPadding+(whole*hBuffer);//apply the buffer and return the value
        })
        .classed("iconPlain",true);

    //console.log(wpvCasesYear)
    d3.select("#txtValue").text( "Total Number of Cases:  " + caseCountForYear);
    d3.selectAll("use").attr("class", function(d,i){
        if (d < (caseCountForYear / 10)) {
            return "iconPlain";
        }
        else {
            return "iconHidden";
        }
    });
}



//Reading and saving wpvCases

//SEAF pattern
//(function(window, $, undefined){
function graphNumWPVCases(){
var listOfCases=[];
d3.json("data/wpvCases.json",function(error,data) {
    if (error) throw error;
    data.forEach(function (entry) {
        var caseYear = {}
        caseYear['dataType'] = 'case';
        caseYear['year'] = entry.YEAR;
        caseYear['value'] = entry;
        listOfCases.push(caseYear);
    });
    //check for year selected by slider
    for(i=0; i<listOfCases.length; i++){
        if(listOfCases[i].year == yearSelectedBySlider){
            var caseYearData = listOfCases[i].value;
            //console.log(caseYearData);
            //initially case Count for this year
            var caseCountForYear = 0;
            //loop through each property of year object
            for(var key in caseYearData){
                if(key!="YEAR"){
                    var value = parseInt(caseYearData[key]);
                    caseCountForYear = caseCountForYear + value;
                }
            }

            window.caseCountGlobal = caseCountForYear;
            //console.log(window.caseCountGlobal);
            drawKidPictogram(caseCountForYear);

        }
    }
});
}
//})(window,jQuery);


//Draw Donation Count
var donationCountForYear = 0;
//.attr("d","M6.347,33.167v-4.03c-2.337-0.04-4.715-0.766-6.125-1.773l0.967-2.699c1.41,0.927,3.506,1.732,5.763,1.732c2.861,0,4.795-1.652,4.795-3.949c0-2.216-1.572-3.586-4.554-4.795c-4.11-1.612-6.649-3.506-6.649-7.012c0-3.345,2.378-5.883,6.045-6.488v-4.03h2.539V4.03c2.378,0.081,4.03,0.726,5.239,1.411l-1.008,2.66C12.513,7.617,10.86,6.69,8.281,6.69c-3.103,0-4.271,1.854-4.271,3.506c0,2.055,1.491,3.103,4.997,4.594c4.151,1.692,6.246,3.748,6.246,7.334c0,3.224-2.216,6.206-6.367,6.851v4.191H6.347z")
//    .attr("transform", "scale(0.3)");
function drawDonationPictogram(donationCountForYear){
    d3.select("#pictogram2").html("");
    //var svgDoc=d3.select("#pictogram").append("svg").attr("viewBox","0 0 293 57"); //factor divide svg by 3.5
    var svgDoc=d3.select("#pictogram2").append("svg").attr("viewBox","0 0 429 57"); //factor divide svg by 3.5
    svgDoc.append("defs")
        .append("g")
        .attr("id","iconDollar")
        .append("path")
        .attr("d","M6.347,33.167v-4.03c-2.337-0.04-4.715-0.766-6.125-1.773l0.967-2.699c1.41,0.927,3.506,1.732,5.763,1.732c2.861,0,4.795-1.652,4.795-3.949c0-2.216-1.572-3.586-4.554-4.795c-4.11-1.612-6.649-3.506-6.649-7.012c0-3.345,2.378-5.883,6.045-6.488v-4.03h2.539V4.03c2.378,0.081,4.03,0.726,5.239,1.411l-1.008,2.66C12.513,7.617,10.86,6.69,8.281,6.69c-3.103,0-4.271,1.854-4.271,3.506c0,2.055,1.491,3.103,4.997,4.594c4.151,1.692,6.246,3.748,6.246,7.334c0,3.224-2.216,6.206-6.367,6.851v4.191H6.347z")
        .attr("transform", "scale(0.2)");
    svgDoc.append("rect")
        .attr("width",1500)
        //.attr("height",200);
    //specify the number of columns and rows for pictogram layout
    var maxCols2 = 50;
    if(donationCountForYear < (maxCols2 * 10)){
        numRows2 =1;
        numCols2 = Math.ceil(donationCountForYear/10);
    }else {
        var numCols2 = 50;
        var numRows2 = Math.ceil(donationCountForYear / 10 / numCols2);
        //console.log(numCols2);
        //console.log(numRows2);
    }

    //padding for grid
    //var xPadding =10;
    //var yPadding =15;
    var xPadding2 =0;
    var yPadding2 =10;
    //horizontal and vertical spacing between the icons
    //var hBuffer = 9;
    //var wBuffer = 8;
    var hBuffer2 = 9;
    var wBuffer2 = 7;
    var myDollarIndex = d3.range(numRows2*numCols2);
    //text element to display number of icons highlighted
    svgDoc.append("text")
        .attr("id","donationTxtValue")
        .attr("x",xPadding2)
        .attr("y",yPadding2)
        .attr("dy",-3)
        .text("WPV Cases");
    //create group element and create an svg <use> element for each icon
    svgDoc.append("g")
        .attr("id","dollarPictoLayer")
        .selectAll("use")
        .data(myDollarIndex)
        .enter()
        .append("use")
        .attr("xlink:href","#iconDollar")
        .attr("id",function(d2)    {
            //console.log('d2:' + d2);
            return "icon"+d2;
        })
        .attr("x",function(d2) {
            var remainder=d2 % numCols2;//calculates the x position (column number) using modulus
            return xPadding2+(remainder*wBuffer2);//apply the buffer and return value
        })
        .attr("y",function(d2) {
            var whole=Math.floor(d2/numCols2)//calculates the y position (row number)
            return yPadding2+(whole*hBuffer2);//apply the buffer and return the value
        })
        .classed("iconPlain",true);

    //console.log(donationCountForYear)
    d3.select("#donationTxtValue").text("Total Money Donated:  " + donationCountForYear.toFixed(2));
    d3.selectAll("#pictogram2 use").attr("class", function(d2,i){
        //console.log("number shown:" + d2);
        if (d2 < (donationCountForYear / 10)) {
            return "iconPlain";
        }
        else {
            return "iconHidden";
        }
    });
}

function graphDonations(){
    d3.json("data/donations_countries.json",function(error,data) {
        //console.log(data);
        var donations = 0;
        for(var key in data){
            if(key==yearSelectedBySlider){
                var yearData = data[key];
                for(var country in yearData){
                    //console.log(donations);
                    //console.log(parseFloat(yearData[country]))
                    donations = donations + parseFloat(yearData[country]);
                }
            }
        }
        //console.log(donations);
        drawDonationPictogram(donations);
    });
}




//function createSVG(){
//
////$("#pictogram").text('pictogram is here');
//
////create svg element
//var svgDoc=d3.select("#pictogram").append("svg").attr("viewBox","0 0 293 57"); //factor divide svg by 3.5
//
//
////define an icon, and store it in svg <defs> elements as a reusable component
//
////var defs = svgDoc.append("defs")
////.append("g")
////.attr("id", "iconCustom");
////
////defs.append("circle")
////        .attr("cx","295.181")
////        .attr("cy","419.561")
////        .attr("cy","2.954")
////defs.append("path")
////        .attr("d","M292.227,423.202")
////defs.append("path")
////        .attr("d","M297.322,423.202l4.523,2.885c0,0,1.856-1.798,1.238-4.575c-0.619-2.775-1.01-5.178-1.147-6.139c-0.138-0.963,0.11-3.585,0.18-3.931c0.069-0.343-2.688-1.504-2.675-5.832c0.016-4.323,3.436-4.053,3.436-4.053s3.298-0.411,3.298,4.603c0,5.018-2.938,4.555-3.214,5.312c-0.274,0.757-0.562,2.183,0.191,5.547c0.756,5.29,1.443,6.252,0,8.793c-1.443,2.542-0.962,1.031-0.962,1.031s0.407,1.79-1.61,1.162c-1.854-1.17-2.777-1.575-2.777-1.575l0.066,13.95c0,0-0.143,1.228-1.19,1.24c-1.048,0.008-1.223-1.018-1.223-1.018v-7.572l-0.728,0.033l-0.027,7.796c0,0-0.137,0.761-1.168,0.761s-1.306-0.897-1.306-0.897v-13.479c0,0-1.373,1.592-1.442,4.34c0,0,0.069,1.17-1.03,1.307c-1.099,0.136-1.168-1.373-1.168-1.373s0.208-3.094,0.756-4.468s2.473-3.848,2.885-3.848C292.64,423.202,297.322,423.202,297.322,423.202z");
//
////define an icon store it in svg <defs> elements as a reusable component - this geometry can be generated from Inkscape, Illustrator or similar
//    svgDoc.append("defs")
//        .append("g")
//        .attr("id","iconCustom")
//        .append("path")
//        .attr("d","M3.5,2H2.7C3,1.8,3.3,1.5,3.3,1.1c0-0.6-0.4-1-1-1c-0.6,0-1,0.4-1,1c0,0.4,0.2,0.7,0.6,0.9H1.1C0.7,2,0.4,2.3,0.4,2.6v1.9c0,0.3,0.3,0.6,0.6,0.6h0.2c0,0,0,0.1,0,0.1v1.9c0,0.3,0.2,0.6,0.3,0.6h1.3c0.2,0,0.3-0.3,0.3-0.6V5.3c0,0,0-0.1,0-0.1h0.2c0.3,0,0.6-0.3,0.6-0.6V2.6C4.1,2.3,3.8,2,3.5,2z");
//
//////define an icon store it in svg <defs> elements as a reusable component - this geometry can be generated from Inkscape, Illustrator or similar
////svgDoc.append("defs")
////    .append("g")
////    .attr("id","iconCustom")
////    .append("path")
////    .attr("d","M297.322,423.202l4.523,2.885c0,0,1.856-1.798,1.238-4.575c-0.619-2.775-1.01-5.178-1.147-6.139c-0.138-0.963,0.11-3.585,0.18-3.931c0.069-0.343-2.688-1.504-2.675-5.832c0.016-4.323,3.436-4.053,3.436-4.053s3.298-0.411,3.298,4.603c0,5.018-2.938,4.555-3.214,5.312c-0.274,0.757-0.562,2.183,0.191,5.547c0.756,5.29,1.443,6.252,0,8.793c-1.443,2.542-0.962,1.031-0.962,1.031s0.407,1.79-1.61,1.162c-1.854-1.17-2.777-1.575-2.777-1.575l0.066,13.95c0,0-0.143,1.228-1.19,1.24c-1.048,0.008-1.223-1.018-1.223-1.018v-7.572l-0.728,0.033l-0.027,7.796c0,0-0.137,0.761-1.168,0.761s-1.306-0.897-1.306-0.897v-13.479c0,0-1.373,1.592-1.442,4.34c0,0,0.069,1.17-1.03,1.307c-1.099,0.136-1.168-1.373-1.168-1.373s0.208-3.094,0.756-4.468s2.473-3.848,2.885-3.848C292.64,423.202,297.322,423.202,297.322,423.202z");
//
////background rectangle
//    svgDoc.append("rect")
//        .attr("width",1024)
//        .attr("height",200);
//
////specify the number of columns and rows for pictogram layout
//    var numCols = 10;
//    var numRows = 3;
//
////padding for grid
//    var xPadding =10;
//    var yPadding =15;
//
////horizontal and vertical spacing between the icons
//    var hBuffer = 15;
//    var wBuffer = 8;
//
////generate a d3 range fro the total number of required elements
//    var myIndex = d3.range(numCols*numRows);
//
////text element to display number of icons highlighted
//    svgDoc.append("text")
//        .attr("id","txtValue")
//        .attr("x",xPadding)
//        .attr("y",yPadding)
//        .attr("dy",-3)
//        .text("WPV Cases");
//
////create group element and create an svg <use> element for each icon
//    svgDoc.append("g")
//        .attr("id","pictoLayer")
//        .selectAll("use")
//        .data(myIndex)
//        .enter()
//        .append("use")
//        .attr("xlink:href","#iconCustom")
//        .attr("id",function(d)    {
//            return "icon"+d;
//        })
//        .attr("x",function(d) {
//            var remainder=d % numCols;//calculates the x position (column number) using modulus
//            return xPadding+(remainder*wBuffer);//apply the buffer and return value
//        })
//        .attr("y",function(d) {
//            var whole=Math.floor(d/numCols)//calculates the y position (row number)
//            return yPadding+(whole*hBuffer);//apply the buffer and return the value
//        })
//        .classed("iconPlain",true);
//
////create a jquery slider to control the pictogram
////$("#sliderDiv").slider({
////    orientation: "horizontal",
////    min: 0,
////    max: numCols*numRows,
////    value: 0,
////    slide: function( event, ui ) {
////        d3.select("#txtValue").text(ui.value);
////        d3.selectAll("use").attr("class",function(d,i){
////            if (d<ui.value)  {
////                return "iconSelected";
////            }    else    {
////                return "iconPlain";
////            }
////        });
////    }
////});
//}
//
//
//
//
//
//
//
//
//function setPictogramCases(){
//    var currentTime = new Date();
//    var month = currentTime.getMonth() + 1;
//    var day = currentTime.getDate();
//    var year = currentTime.getFullYear();
//    var hours = currentTime.getHours();
//    var minutes = currentTime.getMinutes();
//    var seconds = currentTime.getSeconds();
//    console.log(seconds);
//    //console.log("cases!!!!");
//};












