  
var legendDonations = d3.select("#div_legend")
	.append("svg")
	.attr("height", 300);
var dummyDataset = [];

Object.keys(donationColorValues).forEach(function (key) {
	dummyDataset.push({number:key, rgb:donationColorValues[key]});
});
dummyDataset.reverse();
	
	var offset = 15;
	var legend = legendDonations.append("g")
	  .attr("class", "legend");

	legend.selectAll('g')
		.data(dummyDataset)
		.enter()
		.append('g')
      .each(function(d, i) {
        var g = d3.select(this);	
		
        g.append("rect")
          .attr("x", offset)		
          .attr("y", i*25)
          .attr("width", 20)
          .attr("height", 20)
          .style("fill", d.rgb);    

		g.append("text")
          .attr("x", offset + 25)
          .attr("y", i * 25 + 15)
          .attr("height",30)
          .attr("width",100)
          .text(legendTextHelperDonat(d.number));		  
      });	  
	  	  
	legend.append('g')
		  .append("text")
          .attr("x", 0)
          .attr("y", 0)
          .attr("height",30)
          .attr("width",100)
		  .attr("transform", "translate(0,20),rotate(90)" )
          .text("Donations in USD (Millions)");	



function legendTextHelperDonat(number){
	if(number > 200)
		return ">200";
	else if(number == 0)
		return "0"
	else
		return "<" + number;
}