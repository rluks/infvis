/**
 * Created by MIL on 13.01.2016.
 */
(function(){
    updateCaseChart();
})();


function updateCaseChart(){
    d3.select("#caseChart").html("");
    var diameter = 600;

    var svg = d3.select('#caseChart').append('svg')
        .attr('width',diameter)
        .attr('height', diameter);

    var bubble = d3.layout.pack() //create a pack layout
        .size([diameter, diameter])
        .padding(3)// padding between adjacent circles
        .value(function(d) {return d.size;}) // new data will be loaded to bubble layout


    //read data
    //var data = {"countries_msg_vol": {
    //    "CA": 170, "US": 393, "CU": 9, "BR": 89, "MX": 192, "Other": 254
    //}};

    //donations

    //d3.json("data/wpvCases.json",function(error,data) {
    d3.json("https://api.myjson.com/bins/1f9k5",function(error,data) {
        //console.log(data);
        var nodes = bubble.nodes(processCaseData(data))
            .filter(function(d) { return !d.children; }); // filter out the outer bubble


        var vis = svg.selectAll('circle')
            .data(nodes, function(d) { return d.name; });

        vis.enter().append('circle')
            .attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; })
            .attr('r', function(d) { return d.r; })
            .attr('class', function(d) { return d.className; });

        $("#caseChart circle").mouseover(function() {

            var countryName=$(this).attr('class');
            var casesYear = data[selectedYear];
            var countryCode = countryName.toUpperCase();
            var amount = casesYear[countryCode];

            $(this).tooltip(
                {
                    'container': 'body',
                    'placement': 'top'
                }
            ).attr('data-original-title', countryName + ":" + amount).show();
        });

        //on mouse leave, remove tooltip
        $("circle").mouseleave(function() {
            $(this).tooltip('hide');
        });
    });
}

function processCaseData(data){
    console.log('process case data!');
    //var obj = donationData.countries_msg_vol;
    var obj;

    for(var key in data){
        if(key==selectedYear){
            var obj = data[key];
        }
    }

    var newDataSet = [];

    for(var prop in obj) {
        newDataSet.push({name: prop, className: prop.toLowerCase(), size: obj[prop]});
    }
    return {children: newDataSet};
}