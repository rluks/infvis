/**
 * Created by MIL on 13.01.2016.
 */
(function(){
    updateChart();
    //$('[data-toggle="tooltip"]').tooltip();
    //$('#svg').children('circle').each(function () {
    //    var countryName=this.attr('class');
    //    this.attr('data-original-title', countryName);
    //});
})();

var tooltipOpt = {
    title : 'test tooltip'
};


function updateChart(){
    d3.select("#chart").html("");
    var diameter = 600;

    var svg = d3.select('#chart').append('svg')
        .attr('width',diameter)
        .attr('height', diameter);

    var bubble = d3.layout.pack() //create a pack layout
        .size([diameter, diameter])
        .padding(3)// padding between adjacent circles
        .value(function(d) {return d.size;}) // new data will be loaded to bubble layout


    //read data
    //var data = {"countries_msg_vol": {
    //    "CA": 170, "US": 393, "CU": 9, "BR": 89, "MX": 192, "Other": 254
    //}};

    //donations

    //d3.json("data/donations.json",function(error,data) {
    d3.json("https://api.myjson.com/bins/16pot",function(error,data) {
        //console.log(data);
        var nodes = bubble.nodes(processData(data))
            .filter(function(d) { return !d.children; }); // filter out the outer bubble


        var vis = svg.selectAll('circle')
            .data(nodes, function(d) { return d.name; });


        vis.enter().append('circle')
            .attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; })
            .attr('r', function(d) { return d.r; })
            .attr('class', function(d) { return d.className; });

        //on mouse enter, show tooltip
        $("#chart circle").mouseover(function() {

            var countryName=$(this).attr('class');
            //console.log(data);
            var donationsYear = data[selectedYear];
            console.log(donationsYear);
            var countryCode = countryName.toUpperCase();
            console.log(countryCode);
            var amount2 = donationsYear[countryCode].toString();
            console.log(amount2);

            $(this).tooltip(
                {
                    'container': 'body',
                    'placement': 'top'
                }
            ).attr('data-original-title', countryName + ":" + amount2).show();
        });

        //on mouse leave, remove tooltip
        $("circle").mouseleave(function() {
            $(this).tooltip('hide');
        });

    });
}

function processData(data){
    console.log('process donation data!');
    //var obj = donationData.countries_msg_vol;
    var obj;

    for(var key in data){
        if(key==selectedYear){
            var obj = data[key];
        }
    }

    var newDataSet = [];

    for(var prop in obj) {
        newDataSet.push({name: prop, className: prop.toLowerCase(), size: obj[prop]});
    }
    return {children: newDataSet};
}