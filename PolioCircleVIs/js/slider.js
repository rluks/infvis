var selectedYear = 2006;

$(document).ready(function() {
    $("#slider").slider({
        value: 4,
        min: 2006,
        max: 2014,
        step: 1,
        change: function(event, ui) {
            //alert(ui.value);
            selectedYear = ui.value;
            console.log('selected year:' + selectedYear);
            updateChart();
            updateCaseChart();
        }
    })
        .each(function () {

            //
            // Add labels to slider whose values
            // are specified by min, max and whose
            // step is set to 1
            //

            // Get the options for this slider
            var myOpt = $(this).data();
            var uiSlider = myOpt['ui-slider'];
            var opt = uiSlider.options;
            //console.log('test that reading options for the slider works': opt);

            // Get the number of possible values
            var vals = opt.max - opt.min;

            // Space out values
            for (var i = 0; i <= vals; i++) {

                var el = $('<label>' + (i + 2006) + '</label>').css('left', (i / vals * 100) + '%');

                $("#slider").append(el);

            }

        });

    //console.log("test slider");
    });